package main.characters;
import main.exceptions.InvalidArmorException;
import main.exceptions.InvalidWeaponException;
import main.items.*;

import java.util.EnumMap;

public abstract class Character {
    public final String name;
    int level;
    EnumMap<Slot, Item> equipment;
    PrimaryAttributes startingAttributes;
    PrimaryAttributes attributeGainPerLevel;


    public Character(PrimaryAttributes startingAttributes, PrimaryAttributes attributeGainPerLevel, String name) {

        this.name = name;
        this.startingAttributes = startingAttributes;
        this.attributeGainPerLevel = attributeGainPerLevel;
        level = 1;
        this.equipment = new EnumMap<Slot, Item>(Slot.class);

    }

    public void levelUp() {
        this.level += 1;
    }

    //invalidWeaponException and invalidArmorException mut be thrown here.
    public abstract boolean canBeEquipped(WeaponType type);
    public abstract boolean canBeEquipped(ArmorType type);
    public abstract PrimaryAttribute getDamageAttribute();


    public boolean equip(Slot slot, Item item)  throws InvalidArmorException, InvalidWeaponException {
        //this fucked me up cuz there is both weapon and armor at the same place:

        if (item instanceof Weapon) {
            Weapon weapon = (Weapon) item;
            if (!this.canBeEquipped(weapon.type)){
                throw new InvalidWeaponException("Not a valid Weapon for this class");
            }
            if (item.slot != slot){
                throw new InvalidWeaponException("Not a valid Slot");
            }
            if (item.level > this.level ){
                throw new InvalidWeaponException("Your level is not high enough");
            }
        } else if (item instanceof Armor) {
            Armor armor = (Armor) item;
            if (!this.canBeEquipped(armor.type)){
                throw new InvalidArmorException("Not a valid Armor");
            }
            if (item.slot != slot){
                throw new InvalidArmorException("Not a valid Slot");
            }
            if (item.level > this.level ){
                throw new InvalidArmorException("Your level is not high enough");
            }
        }
        this.equipment.put(slot, item);
        return true;

    }

    public int getTotalAttribute(PrimaryAttribute attribute){
        // weapon not included, since it has damage and does not add to attributes.

        int levelFactor = this.level - 1; // for the factoring attribute per level.starting level 2
        Armor head = (Armor) this.equipment.get(Slot.HEAD);
        Armor body = (Armor) this.equipment.get(Slot.BODY);
        Armor legs = (Armor) this.equipment.get(Slot.LEGS);

        int desiredAttributePoints = startingAttributes.get(attribute) +
                attributeGainPerLevel.get(attribute)*levelFactor;
        desiredAttributePoints += head == null ? 0 : head.attributes.get(attribute);
        desiredAttributePoints += body == null ? 0 : body.attributes.get(attribute);
        desiredAttributePoints += legs == null ? 0 : legs.attributes.get(attribute);

        return desiredAttributePoints;

    }

    public float getDamagePerSecond() {

        // getting weapon we have:
        Weapon weapon = (Weapon) this.equipment.get(Slot.WEAPON);

        // getting the total point of damage attribute.
        int damageAttribute = this.getTotalAttribute(this.getDamageAttribute());
        // setting the 10% damage, that damage attribute adds to the DPS.
        float modifier = 100 + damageAttribute;
        // setting damage dealt by a weapon, no weapon still deals 1 damage
        float damage = weapon == null ? 1 : weapon.damage;
        // increased damage by damage attribute:
        damage *= modifier/ 100;

        float speed = weapon == null ? 1 : weapon.speed;
        return damage * speed;
    }

    public String getStats(){
        // don't really know why I am using this StringBuilder,
        // but let us follow the task;
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Name: " + this.name);
        stringBuilder.append(" Level: " + this.level);
        stringBuilder.append(" Strength: " + this.getTotalAttribute(PrimaryAttribute.STRENGTH));
        stringBuilder.append(" Dexterity: " + this.getTotalAttribute(PrimaryAttribute.DEXTERITY)) ;
        stringBuilder.append(" Intelligence: " + this.getTotalAttribute(PrimaryAttribute.INTELLIGENCE));
        stringBuilder.append(" Vitality: " + this.getTotalAttribute(PrimaryAttribute.VITALITY));
        stringBuilder.append(" DPS: " + this.getDamagePerSecond());

        return stringBuilder.toString();
    }
}
