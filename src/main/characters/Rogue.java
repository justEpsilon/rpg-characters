package main.characters;

import main.items.*;

public class Rogue extends Character {

    public Rogue(String name) {
        super(new PrimaryAttributes(8, 2,6,1),
                new PrimaryAttributes(3,1,4,1), name);
    }

    @Override
    public boolean canBeEquipped(WeaponType type)  {
        return type == WeaponType.DAGGER || type == WeaponType.SWORD;
    }

    @Override
    public boolean canBeEquipped(ArmorType type) {
        return type == ArmorType.LEATHER || type == ArmorType.MAIL;
    }

    @Override
    public PrimaryAttribute getDamageAttribute() {
        return PrimaryAttribute.DEXTERITY;
    }
}
