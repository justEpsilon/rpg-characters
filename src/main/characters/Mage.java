package main.characters;

import main.items.*;

public class Mage extends Character {

    public Mage(String name) {
        super(new PrimaryAttributes(5, 1, 1, 8),
                new PrimaryAttributes(3, 1, 1, 5), name);
    }

    @Override
    public boolean canBeEquipped(WeaponType type) {
        return type == WeaponType.STAFF || type == WeaponType.WAND;
    }

    @Override
    public boolean canBeEquipped(ArmorType type) {
        return type == ArmorType.CLOTH;
    }

    @Override
    public PrimaryAttribute getDamageAttribute() {
        return PrimaryAttribute.INTELLIGENCE;
    }
}
