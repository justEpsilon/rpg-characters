package main.characters;

import main.items.*;

public class Warrior extends Character{
    public Warrior(String name) {
        super(new PrimaryAttributes(10, 5,2,1),
                new PrimaryAttributes(5,3,2,1), name);
    }

    @Override
    public boolean canBeEquipped(WeaponType type) {
        return type == WeaponType.AXE || type == WeaponType.HAMMER || type == WeaponType.SWORD;
    }

    @Override
    public boolean canBeEquipped(ArmorType type) {
        return type == ArmorType.MAIL || type == ArmorType.PLATE;
    }

    @Override
    public PrimaryAttribute getDamageAttribute() {
        return PrimaryAttribute.STRENGTH;
    }
}
