package main.characters;

import java.util.EnumMap;

public class PrimaryAttributes {

    EnumMap<PrimaryAttribute, Integer> attributes;

    public PrimaryAttributes(int vitality, int strength, int dexterity, int intelligence ){
        this.attributes = new EnumMap<PrimaryAttribute, Integer>(PrimaryAttribute.class);
        this.attributes.put(PrimaryAttribute.VITALITY, vitality);
        this.attributes.put(PrimaryAttribute.STRENGTH, strength);
        this.attributes.put(PrimaryAttribute.DEXTERITY, dexterity);
        this.attributes.put(PrimaryAttribute.INTELLIGENCE, intelligence);
    }

    public int get(PrimaryAttribute attribute) {
        return this.attributes.get(attribute);
    }

}
