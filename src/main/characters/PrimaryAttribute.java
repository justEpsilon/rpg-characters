package main.characters;

public enum PrimaryAttribute {
    VITALITY, STRENGTH, DEXTERITY, INTELLIGENCE
}
