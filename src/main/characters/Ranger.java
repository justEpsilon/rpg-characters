package main.characters;

import main.items.*;

public class Ranger extends Character{

    public Ranger(String name) {
        super(new PrimaryAttributes(8, 1, 7, 1),
                new PrimaryAttributes(2, 1, 5, 1),name);

    }


    @Override
    public boolean canBeEquipped(WeaponType type) {
        return type == WeaponType.BOW;
    }

    @Override
    public boolean canBeEquipped(ArmorType type) {
        return type == ArmorType.LEATHER || type == ArmorType.MAIL;
    }

    @Override
    public PrimaryAttribute getDamageAttribute() {
        return PrimaryAttribute.DEXTERITY;
    }
}
