package main;

import main.characters.Character;
import main.characters.Mage;
import main.characters.PrimaryAttributes;
import main.exceptions.InvalidArmorException;
import main.exceptions.InvalidWeaponException;
import main.items.*;

public class Main {

    public static void main(String[] args) {

        Character juste = new Mage("juste");
        System.out.println(juste.getDamagePerSecond());
        juste.levelUp();
        Item littleHat = new Armor("hat",
                2,
                Slot.HEAD,
                new PrimaryAttributes(1,1,1,1),
                ArmorType.CLOTH);

        try {
            juste.equip(Slot.HEAD, littleHat);
            System.out.println(juste.getStats());
        }catch (InvalidArmorException | InvalidWeaponException e){}

    }

}
