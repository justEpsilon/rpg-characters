package main.items;

public class Weapon extends Item {
    public final int damage;
    public final float speed; // attacks per second
    public WeaponType type;

    public Weapon(String name, int level, int damage, float speed, WeaponType type) {
        super(name, level, Slot.WEAPON);
        this.damage = damage;
        this.speed = speed;
        this.type = type;
    }

    public void setType(WeaponType type) {
        this.type = type;
    }
}
