package main.items;

public enum ArmorType {
    CLOTH, LEATHER, MAIL, PLATE
}
