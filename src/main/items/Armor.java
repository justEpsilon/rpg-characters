package main.items;

import main.characters.PrimaryAttributes;

public class Armor extends Item {

    public final PrimaryAttributes attributes;
    public ArmorType type;


    public Armor(String name, int level, Slot slot, PrimaryAttributes attributes, ArmorType type) {
        super(name, level, slot);
        this.attributes = attributes;
        this.type = type;
    }

    public void setType(ArmorType armorType){
        this.type = armorType;
    }
}
