package main.items;

public enum WeaponType {
    AXE, BOW, DAGGER, HAMMER, STAFF, SWORD, WAND
}
