package main.items;

public abstract class Item {
    public final String name;
    public final int level; // this is the level at witch the item can be equiped by the character.
    public final Slot slot;


    public Item(String name, int level, Slot slot) {
        this.name = name;
        this.level = level;
        this.slot = slot;
    }


}
