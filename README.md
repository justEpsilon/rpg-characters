# Console application in Java

This is a backend for an RPG game.

## Objectives: 

Demonstrate usage of object-oriented programming techniques. Such as polymorphism implemented with inheritance.   

## How to run: 

This program is made in IntelliJ, the only way it is "meant" to be run is through it's tests folder. Simply run all the unittests in the tests folder, and verify that the program works as intended. You can write a little test characters in main folder and run MAIN aswell.

### Notes:

Character class is an abstract class extending different type of characters, and most of the methods are confined in the Character class, therefore not all the tests are run for eatch character, but only for Mage. Otherwise most of the test are in CharachterTests class.
