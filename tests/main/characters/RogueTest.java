package main.characters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RogueTest {

    @Test
    void testStartingAttributes(){
        Rogue rogue = new Rogue("Rogue");
        assertEquals(8,rogue.getTotalAttribute(PrimaryAttribute.VITALITY));
        assertEquals(2,rogue.getTotalAttribute(PrimaryAttribute.STRENGTH));
        assertEquals(6,rogue.getTotalAttribute(PrimaryAttribute.DEXTERITY));
        assertEquals(1,rogue.getTotalAttribute(PrimaryAttribute.INTELLIGENCE));
    }
}