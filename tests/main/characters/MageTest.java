package main.characters;

import main.items.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

// if test pass for Mage should also pass for all the other characters, since they are analogous.
class MageTest {

    @Test
    void canBeEquipped() {
        Character character = new Mage("Big Mage");
        boolean canThisBeEquiped = character.canBeEquipped(WeaponType.STAFF);
        assertEquals(true, canThisBeEquiped);

        canThisBeEquiped = character.canBeEquipped(WeaponType.WAND);
        assertEquals(true, canThisBeEquiped);

        canThisBeEquiped = character.canBeEquipped(WeaponType.AXE);
        assertEquals(false, canThisBeEquiped);

        canThisBeEquiped = character.canBeEquipped(WeaponType.BOW);
        assertEquals(false, canThisBeEquiped);

        canThisBeEquiped = character.canBeEquipped(WeaponType.DAGGER);
        assertEquals(false, canThisBeEquiped);

    }

    @Test
    void testCanBeEquipped() {

        Character character = new Mage("Big Mage");
        boolean canThisBeEquiped = character.canBeEquipped(ArmorType.CLOTH);
        assertEquals(true, canThisBeEquiped);

        canThisBeEquiped = character.canBeEquipped(ArmorType.LEATHER);
        assertEquals(false, canThisBeEquiped);


    }

    @Test
    void getDamageAttribute() {
        Character character = new Mage("Big Mage");
        character.levelUp(); //mages damage attribute is intelligence
        PrimaryAttribute damage = character.getDamageAttribute();

        assertEquals(PrimaryAttribute.INTELLIGENCE, damage);
    }
    @Test
    void testStartingAttributes(){
        Mage mage = new Mage("Mage");
        assertEquals(5,mage.getTotalAttribute(PrimaryAttribute.VITALITY));
        assertEquals(1,mage.getTotalAttribute(PrimaryAttribute.STRENGTH));
        assertEquals(1,mage.getTotalAttribute(PrimaryAttribute.DEXTERITY));
        assertEquals(8,mage.getTotalAttribute(PrimaryAttribute.INTELLIGENCE));
    }
}