package main.characters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangerTest {

    @Test
    void testStartingAttributes(){
        Ranger ranger = new Ranger("Ranger");
        assertEquals(8,ranger.getTotalAttribute(PrimaryAttribute.VITALITY));
        assertEquals(1,ranger.getTotalAttribute(PrimaryAttribute.STRENGTH));
        assertEquals(7,ranger.getTotalAttribute(PrimaryAttribute.DEXTERITY));
        assertEquals(1,ranger.getTotalAttribute(PrimaryAttribute.INTELLIGENCE));
    }
}
