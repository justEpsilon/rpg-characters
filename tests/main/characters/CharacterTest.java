package main.characters;

import main.exceptions.InvalidArmorException;
import main.exceptions.InvalidWeaponException;
import main.items.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {

    @Test
    void levelUp() {
        Character character = new Mage("Juste");

        character.levelUp();
        character.levelUp();
        character.levelUp();

        assertEquals(4, character.level);
    }

    @Test
    void canBeEquipped() {

        Character character = new Warrior("Big Boi");
        boolean canThisBeEquipped = character.canBeEquipped(WeaponType.AXE);
        assertEquals(true, canThisBeEquipped);

    }


    @Test
    void equip() throws InvalidArmorException, InvalidWeaponException {

        final Character juste = new Mage("Juste");

        juste.levelUp();
        Armor testHat = new Armor("Hat",
                2,
                Slot.HEAD,
                new PrimaryAttributes(1,1,1,1),
                ArmorType.CLOTH);

        boolean isEquipped = juste.equip(Slot.HEAD,testHat);
        assertEquals(true, isEquipped);

        assertThrows(InvalidArmorException.class, () -> juste.equip(Slot.BODY,testHat) ); //invalid Slot

        final Character johan = new Warrior("Johan");
        assertThrows(InvalidArmorException.class, () -> johan.equip(Slot.HEAD,testHat) );//invalid armortype

        testHat.setType(ArmorType.MAIL);// hat made of paper. you know mail!

        assertThrows(InvalidArmorException.class, () -> johan.equip(Slot.HEAD,testHat) );//to high level

        johan.levelUp();

        isEquipped = johan.equip(Slot.HEAD,testHat);
        assertEquals(true,isEquipped);


    }

    @Test
    void getDamagePerSecond() throws InvalidWeaponException,InvalidArmorException {

        Weapon weapon = new Weapon("Big Axe",2,3, 1.5F, WeaponType.AXE);
        Character character = new Warrior("Johan");
        float DPS = character.getDamagePerSecond();

        float maxError = 1e-5F;
        boolean isWithinBounds = Math.abs(DPS - 1.05F ) < maxError;// no weapon
        assertTrue(isWithinBounds);

        character.levelUp();
        character.equip(Slot.WEAPON, weapon);
        DPS = character.getDamagePerSecond();

        isWithinBounds = Math.abs(DPS - 4.86F )< maxError; // 4.86 is the DPS expected from this level warrior carrying this weapon.
        assertTrue(isWithinBounds);

        Armor helmet = new Armor("Big Metal Hat",
                2,
                Slot.HEAD,
                new PrimaryAttributes(0,10,0,0),
                ArmorType.PLATE);

        character.equip(Slot.HEAD,helmet);
        DPS = character.getDamagePerSecond();

        isWithinBounds = Math.abs(DPS - 5.31F ) < maxError; // 5.31 is the DPS expected from this level warrior carrying this weapon and hat.
        assertTrue(isWithinBounds);

    }

    @Test
    void getTotalAttribute() throws InvalidWeaponException, InvalidArmorException {

        Rogue johan = new Rogue("Your Hand");
        Weapon anduril = new Weapon(
                "Anduril, reforged from the shards of Narsil",4,
                20, 2.1F,WeaponType.SWORD);
        Armor letterMansArmor = new Armor("Letterman's Armor",4,Slot.BODY,
                new PrimaryAttributes(2,3,6,5),ArmorType.MAIL);
        Armor drMartens = new Armor("Doctor Martens Adrian Leather Tassel Loafers",2,Slot.LEGS,
                new PrimaryAttributes(5,0,3,-1), ArmorType.LEATHER);
        Armor leatherPilotHat = new Armor("One of those oldschool leather helmets that pilots used to wear",
                4,Slot.HEAD, new PrimaryAttributes(1,0,4,2), ArmorType.LEATHER);

        johan.levelUp();
        johan.levelUp();
        johan.levelUp();

        johan.equip(Slot.HEAD,leatherPilotHat);
        johan.equip(Slot.BODY,letterMansArmor);
        johan.equip(Slot.LEGS,drMartens);
        johan.equip(Slot.WEAPON,anduril);//this doesn't even give anything

        assertEquals(25,johan.getTotalAttribute(PrimaryAttribute.VITALITY));
        assertEquals(8,johan.getTotalAttribute(PrimaryAttribute.STRENGTH));
        assertEquals(31,johan.getTotalAttribute(PrimaryAttribute.DEXTERITY));
        assertEquals(10,johan.getTotalAttribute(PrimaryAttribute.INTELLIGENCE));

    }
}