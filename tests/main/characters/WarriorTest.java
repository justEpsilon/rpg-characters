package main.characters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {

    @Test
    void testStartingAttributes(){
        Warrior warrior = new Warrior("Warrior");
        assertEquals(10,warrior.getTotalAttribute(PrimaryAttribute.VITALITY));
        assertEquals(5,warrior.getTotalAttribute(PrimaryAttribute.STRENGTH));
        assertEquals(2,warrior.getTotalAttribute(PrimaryAttribute.DEXTERITY));
        assertEquals(1,warrior.getTotalAttribute(PrimaryAttribute.INTELLIGENCE));
    }

}